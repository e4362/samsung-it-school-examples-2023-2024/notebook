package ru.dolbak.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    Button button;
    TextView textView;
    EditText editText;

    String FILE_NAME = "notes.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
        editText = findViewById(R.id.editTextText);
        button = findViewById(R.id.button);
        readFile();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeFile();
                readFile();
            }
        });
    }

    void readFile(){
        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            openFileInput(
                                    FILE_NAME
                            )
                    )
            );
            String str= br.readLine();
            textView.setText(str);
            br.close();

        } catch (FileNotFoundException e) {
            textView.setText("Файл на найден");
        } catch (IOException e) {
            textView.setText("Ошибка чтения");
        }
    }
    void writeFile(){
        try {
            BufferedWriter bw = new BufferedWriter(
                    new OutputStreamWriter(
                            openFileOutput(FILE_NAME,
                                    MODE_PRIVATE)
                    )
            );
            bw.write(editText.getText().toString());
            editText.setText("");
            bw.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}